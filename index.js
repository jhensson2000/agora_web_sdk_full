const db= firebase.firestore();

const tabla_reuniones = document. getElementById('tabla-reuniones');

const getReuniones = () => db.collection('reuniones').get()

const deleteReunion = id => db.collection('reuniones').doc(id).delete()

window.addEventListener('DOMContentLoaded', async(e) =>{
    const querySnapshot= await getReuniones()
    querySnapshot.forEach(doc =>{
        const data = doc.data()
        console.log(data)

        var fecha_creacion= new Date(data.fecha_creacion)
        var fecha_expiracion= new Date(data.fecha_expiracion)
        tabla_reuniones.innerHTML+= 
        '<tr>'+
            '<td>'+data.nombre+'</td>'+
            '<td>'+data.channelName+'</td>'+
            '<td>'+fecha_creacion.toLocaleDateString()+' '+fecha_creacion.toLocaleTimeString()+'</td>'+
            '<td>'+fecha_expiracion.toLocaleDateString()+' '+fecha_expiracion.toLocaleTimeString()+'</td>'+
            //'<td>'+data.token+'</td>'+
            '<td>'+
                '<a href="main-reunion.html?id_reunion='+doc.id+'" data-id="'+doc.id+'">Ingresar</a> '+
                '<a href="#" class="delete-reunion" data-id="'+doc.id+'">Eliminar</a>'+
            '</td>'+
        '</tr>'

        const btnsDelete = document.querySelectorAll('.delete-reunion')
        btnsDelete.forEach(btn =>{
            btn.addEventListener('click', async(e) => {
                await deleteReunion(e.target.dataset.id)
                alert("Reunión eliminada")
                location.reload()
            })
        })
    })
})