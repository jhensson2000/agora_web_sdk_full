const db= firebase.firestore();

const form = document.getElementById('form-reunion');

const uid= "0"
form.addEventListener('submit', async(e) =>{
    e.preventDefault()

    const channelName = "doctor"+ Date.now()
    const expirationTimeInSeconds = form['tiempo_expiracion'].value*60
    console.log("Tiempo de expiración: "+expirationTimeInSeconds + "s")

    const nombre= form['reunion'].value
    const fecha_creacion = Date.now()
    const fecha_expiracion = Date.now() +(expirationTimeInSeconds*1000)
    
    //const host= "http://127.0.0.1:8080"
    const host= "https://35.169.255.89:9997"

    fetch(host+'/rtcToken?channelName='+channelName+"&uid="+uid+"&expirationTimeInSeconds="+expirationTimeInSeconds)
    .then(function(response) {
        response.json().then(async(object) => {
            const response = await db.collection('reuniones').doc().set({
                nombre,
                channelName,
                token: object.key,
                fecha_creacion,
                fecha_expiracion
            })
        
            alert("Reunión creada")
        
            location.href = "index.html"
        })
    })

    
})



